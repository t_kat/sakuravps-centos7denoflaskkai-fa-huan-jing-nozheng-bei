# CentOS7におけるFlask開発環境の設定

## 1. 環境
* さくらVPS CentOS7.2.1511
* Apache2.4

## 2.各種開発/実行に必要なパッケージのインストール

### 2-1. yumパッケージ追加
1. リポジトリの追加「yum install -y https://centos7.iuscommunity.org/ius-release.rpm」実行。
2. Python3.6追加「yum -y install python36」実行。
3. Pythonモジュールの構築に必須なファイル、ライブラリが含まれたPython3.6追加「yum -y install python36-devel」。
4. Apache2追加「yum -y install httpd」実行。
5. GCC追加「yum -y install gcc」実行。
6. XMLパーサ追加「yum - y install expat-devel」実行。

### 2-2. pipパッケージ追加
1. WSGI追加「python36 -m pip install mod_wsgi-httpd」実行。
2. WSGI追加その2「python36 -m pip install mod_wsgi」実行。
3. Flask追加「python36 -m pip install flask」

## 3. Applicationファイルと設定ファイルの作成

### 3-1. Flaskプロジェクトディレクトリの作成とapp.pyの作成
1. Apache2ホームディレクトリ（/var/www）直下ににflaskディレクトリを作成する。（/var/www/flask）
2. flaskディレクトリ直下に「app.py」を作成する。ファイルの中身は以下とする。

```
    ---------------------------------------------------
    from flask import Flask
    app = Flask(__name__)

    @app.route("/")
    def index():
        return "Hello, Worldだよ(*˘︶˘*).｡.:*♡"

    if __name__ == "__main__":
        app.debug = True
        app.run(host='0.0.0.0', port=80)
    ---------------------------------------------------
```

### 3-2. WSGIとの連携ファイルの作成
1. wsgiとのアダプターファイル「adapter.py」をflaskディレクトリ直下に作成する。ファイルの中身は以下とする。

```
    ---------------------------------------------------
    # coding: utf-8
    import sys
    sys.path.insert(0, "/var/www/flask")

    from app import app as application
    ---------------------------------------------------
```

### 3-3. Apache2の設定ファイルの作成
1. ディレクトリ「/etc/httpd/conf.d」直下に「flask.conf」を作成。ファイルの中身は以下とする。

```
    ---------------------------------------------------
    LoadModule wsgi_module /usr/local/lib64/python3.6/site-packages/mod_wsgi/server/mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so

    <VirtualHost *:80>
    WSGIDaemonProcess snapmsg user=apache group=apache threads=5 python-home=/var/www/flask
    WSGIScriptAlias / /var/www/flask/adapter.wsgi
    WSGIScriptReloading On

    <Directory “/var/www/flask“>
    WSGIProcessGroup flask
    WSGIApplicationGroup %{GLOBAL}
    AllowOverride All
    Require all granted
    </Directory>
    </VirtualHost>
    ---------------------------------------------------
```

* 1行目：LoadModuleのあとに「mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so」ファイルの場所を記載する。
* 5行目：「python-home=」の右側はFlaskのプロジェクトディレクトリを指定。
* 6行目：「/」と「wsgi設定ファイル」の場所を記載する。 ※ 本例では、「/var/www/flask/adapter.wsgi」
* 9行目：「Flaskのプロジェクトディレクトリを指定」。

## 4.WEBサーバの実行とFirewallの設定

### 4-1. Apache2の起動
1. 「systemctl start httpd」実行。

### 4-2. Firewallの設定
1. http通信の通過設定「firewall-cmd --add-service=http --zone=public --permanent」実行。
2. https通信の通過設定「firewall-cmd --add-service=https --zone=public --permanent」実行。
3. Firewallの再起動「systemctl restart firewalld」実行。

### 4-3. 稼働のテスト
1. ブラウザで「http://xxx.xxx.xxx.xxx（VPSのアドレス）」にアクセスする。「Hello, Worldだよ(*˘︶˘*).｡.:*♡」と表示されればOK！。

## 参考文献
* [Flask公式](http://flask.pocoo.org/docs/1.0/)
* [Flask公式 -mod_wsgiについて-](http://flask.pocoo.org/docs/1.0/deploying/mod_wsgi/)
* [ネコでもわかる！さくらのVPS講座 ～第三回「Apacheをインストールしよう」](https://knowledge.sakura.ad.jp/8541/)

